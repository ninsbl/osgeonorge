from osgeonorge.utils import norwegian_to_ascii


def test_example():
    assert 1 == 1


def test_norwegian_to_ascii():
    assert norwegian_to_ascii("Gjærsjømåke") == 'Gjarsjomake'

"""
def test_helloworld_helloworld():
    assert HelloWorld.helloworld('Chris') == 'Hello World Chris!'


def test_fizzbuzz(capsys):
    fizzbuzz(9)
    captured = capsys.readouterr()
    assert captured.out == '1\n2\nFizz\n4\nBuzz\nFizz\n7\n8\nFizz\n'

"""

