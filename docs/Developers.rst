======================
Developers
======================

OSGeonorge is under active development and there is
plenty of room for possible improvements. If you
have any ideas for new features or discover a bug,
please file a `merge request <gitlab_mr>`_ or open
an `issue <gitlab_issues>`_ on gitlab.


--------
Source code
--------

OSGeonorge is Free and Open Source Software with a
GPL >= 3 license. The source code is available on
gitlab_. 

See CONTRIBUTING.md

.. _gitlab: https://gitlab.com/ninsbl/osgeonorge
.. _gitlab_issues: https://gitlab.com/ninsbl/osgeonorge/-/issues
.. _gitlab_mr: https://gitlab.com/ninsbl/osgeonorge/-/merge-requests
