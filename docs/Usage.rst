=====
Usage
=====

The OSGeonorge package can be used for various tasks


--------
Listing ATOM feeds and content
--------

For interactive usage a first step is often to list avaiable
data, formats and coverage

.. code-block:: python

   # import module
   from osgeonorge.geonorge import GeonorgeAdapter
   # Initialize class
   ogna = GeonorgeAdapter(cores=5)
   ogna.list_feeds()


--------
Downloading data 
--------

In a next step selected data can be downoaded
If multiple datasets are selected and more than one core is
allocated to the task, OSGeonorge will download files
in parallel.

.. code-block:: python

   ogna.get_feed("FKB_Bygning", "FGDB")
   ogna.get_data_dict("Landsdekkende")
   ogna.download_data()


--------
Extracting metadata for import 
--------

In case data needs transformation before import,
OSGeonorge allows to extract relevant metadata
from all or selected layers.

.. code-block:: python

   ogna.get_metadata()


--------
Downloadm extract and parse metadata for import in one step
--------

In case data needs transformation before import,
OSGeonorge allows to extract relevant metadata
from all or selected layers.

.. code-block:: python

   # import module
   from osgeonorge.geonorge import GeonorgeAdapter
   # Initialize class
   ogna = GeonorgeAdapter(cores=5)
   ogna.fetch("FKB_Bygning", "FGDB", "Landsdekkende")


--------
Command line 
--------

OSGeonorge is primarily written as a Python library. However,
it provides a command line (CLI) interface as well. Currently,
only listing of ATOM feeds is supported in the CLI interface.

.. code-block:: shell

   list_atom


