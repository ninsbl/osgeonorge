.. Python Package Template documentation master file, created by
   sphinx-quickstart on Fri Mar 30 08:47:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OSGeonorge's documentation!
===================================================

Norway provieds an amazing amount of public and often freely available
spatial data. That effort of different data providers is coordinated 
by the Norwegian Mapping Authority and made available through the
Geonorge platform. Geonorge provides an API for downloading data
using ATOM feeds.

The OSGeo community provides a trove of highly capable software to read,
transform, store, analyse and present that kind of data.

The OSGeonorge package is a collection of tools and functions for ETL
or ELT tasks with data from Geonorge.no using OSGeo software. It aims
at simplifying data warehousing or inclusion of data from geonorge in
maintenance or analysis pipelines.


.. toctree::

   Installation
   Usage
   source/osgeonorge
   Developers

   :ref:`genindex`
   :ref:`modindex`


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
