FROM alpine:latest

LABEL authors="Stefan Blumentrath"
LABEL maintainer="stefan.blumentrath@nina.no"
MAINTAINER Stefan Blumentrath

ARG VERSION=v0.1.9
ARG USERNAME=ninsbl
ARG PROJECT=osgeonorge

RUN apk add grass-dev gdal-dev py3-gdal py3-numpy py3-psycopg2 py3-requests py3-pip
# Download package, install package, no cache
RUN pip install --no-cache-dir grass_session
RUN pip install --no-cache-dir https://gitlab.com/$USERNAME/$PROJECT/-/archive/$VERSION/osgeonorge-${VERSION}.tar.gz

ENTRYPOINT ["list_atom"]
CMD ["list_atom"]
