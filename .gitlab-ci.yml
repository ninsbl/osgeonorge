variables:
    DOCKER_USERNAME: ninsbl
    DOCKER_PASSWORD: $DOCKER_PASSWORD
    TWINE_USERNAME: $TWINE_USERNAME
    TWINE_PASSWORD: $TWINE_PASSWORD
    ANACONDA_USERNAME: $ANACONDA_USERNAME
    ANACONDA_TOKEN: $ANACONDA_TOKEN

stages:
  - test
  - deploy
  - docs

test:
  image: python:3.9
  stage: test
  script:
    - echo "$pypirc"
    - echo $TWINE_USERNAME
    - echo $TWINE_PASSWORD | wc -c
    - apt-get -qq update && apt-get install -y libgdal-dev grass-dev
    - export GDAL_VERSION=$(gdal-config --version)
    - pip install --upgrade --no-cache-dir setuptools==57.5.0  # GDAL build only runs with setuptools < 58
    - pip install requests psycopg2 GDAL==${GDAL_VERSION} grass_session numpy pytest pytest-cov
    - pip install .
    - pytest --cov=osgeonorge tests

deploy_pypi:
  image: python:3.9
  stage: deploy
  script:
    - echo $TWINE_USERNAME
    - echo $TWINE_PASSWORD | wc -c
    - apt-get -qq update && apt-get install -y libgdal-dev grass-dev
    - export GDAL_VERSION=$(gdal-config --version)
    - pip install --upgrade --no-cache-dir setuptools==57.5.0  # GDAL build only runs with setuptools < 58
    - pip install requests psycopg2 GDAL==${GDAL_VERSION} grass_session numpy pytest pytest-cov
    - python3 -m pip install -U twine build setuptools
    - python3 -m build
    - python3 -m pip install ./
    - export "TWINE_REPOSITORY_URL=https://test.pypi.org/legacy/"
    - python3 -m twine upload --verbose dist/*.tar.gz
    - python3 -m twine upload --verbose dist/*.whl
    - export "TWINE_REPOSITORY_URL=https://upload.pypi.org/legacy/"
    - python3 -m twine upload --verbose dist/*.tar.gz
    - python3 -m twine upload --verbose dist/*.whl
  only:
    - /^v\d+\.\d+\.\d+([abc]\d*)?$/  # PEP-440 compliant version (tags)


deploy_conda:
  image: continuumio/miniconda3:latest
  stage: deploy
  script:
    - conda install anaconda-client setuptools conda-build -y
    - python setup.py sdist bdist_conda
    - anaconda -t $ANACONDA_TOKEN upload -u $ANACONDA_USERNAME /opt/conda/conda-bld/linux-64/osgeonorge*.tar.bz2
  only:
    - /^v\d+\.\d+\.\d+([abc]\d*)?$/  # PEP-440 compliant version (tags)


deploy_docker:
  image: docker:git
  stage: deploy
  services:
    - docker:dind
  script:
    - docker build -t osgeonorge:$CI_COMMIT_TAG --build-arg VERSION=$CI_COMMIT_TAG .
    # push to dockerhub
    - docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
    - docker tag osgeonorge:$CI_COMMIT_TAG ninsbl/osgeonorge:$CI_COMMIT_TAG
    - docker tag osgeonorge:$CI_COMMIT_TAG ninsbl/osgeonorge:latest
    - docker push ninsbl/osgeonorge:$CI_COMMIT_TAG
    - docker push ninsbl/osgeonorge:latest
    # push to gitlab registry
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker tag osgeonorge:$CI_COMMIT_TAG registry.gitlab.com/ninsbl/osgeonorge:$CI_COMMIT_TAG
    - docker tag osgeonorge:$CI_COMMIT_TAG registry.gitlab.com/ninsbl/osgeonorge:latest
    - docker push registry.gitlab.com/ninsbl/osgeonorge:$CI_COMMIT_TAG
    - docker push registry.gitlab.com/ninsbl/osgeonorge:latest
  only:
    - /^v\d+\.\d+\.\d+([abc]\d*)?$/  # PEP-440 compliant version (tags)

pages:
  image: python:3.9
  stage: docs
  script:
    - apt-get -qq update && apt-get install -y libgdal-dev grass-dev
    - export GDAL_VERSION=$(gdal-config --version)
    - pip install --upgrade --no-cache-dir setuptools==57.5.0  # GDAL build only runs with setuptools < 58
    - pip install sphinx sphinx_rtd_theme requests psycopg2 GDAL==${GDAL_VERSION} grass_session
    - pip install -e .
    - mkdir public
    - cd docs
    - make apidocs
    - make html
    - cp -r _build/html/* ../public
  artifacts:
    paths:
      - public
  only:
    - main
