# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.2]
### Changed
- PostGISAdapter: Fix wrong use of tabs

## [0.2.1]
### Added
- GeonorgeAdapter: support of Celle coverage
- PostGISAdapter: Possibility to connect with password

### Changed
- GeonorgeAdapter: Make authentication optional (with warning)

### Removed

## [0.2.0]
### Added
- GRASSGISAdapter

### Changed
- PostGISAdapter
- GeonorgeAdapter

### Removed

## [Unreleased]

## [0.1.9]
2nd test release

## [0.1.8]
1st test release

